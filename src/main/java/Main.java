import dataBase.DataBaseConnection;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.sql.SQLException;

public class Main {

    private static final Logger Log = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws XMLStreamException, JAXBException, SQLException {
        DataBaseConnection dataBaseConnection = new DataBaseConnection();
        dataBaseConnection.init();

        Log.info("File parsing start");
        String fileName = "src\\main\\resources\\RU-NVS.osm";
        XMLReader.parseXMLfile(fileName, dataBaseConnection.getConnection());
        Log.info("File parsing end");
        try {
            dataBaseConnection.closeConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
