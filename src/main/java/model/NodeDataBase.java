package model;

import lombok.Getter;
import lombok.Setter;
import model.generated.org.openstreetmap.osm._0.Node;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class NodeDataBase {
    private final long id;
    private String user;
    private final long uid;
    private final long version;
    private final Double longitude;
    private final Double latitude;
    private final List<TagDataBase> tags;

    public NodeDataBase(long id, String user, long uid, long version, Double longitude, Double latitude, List<TagDataBase> tags) {
        this.id = id;
        this.user = user;
        this.uid = uid;
        this.version = version;
        this.longitude = longitude;
        this.latitude = latitude;
        this.tags = tags;
    }

    public NodeDataBase(long id, String user, long uid, long version, Double longitude, Double latitude) {
        this.id = id;
        this.user = user;
        this.uid = uid;
        this.version = version;
        this.longitude = longitude;
        this.latitude = latitude;
        this.tags = new ArrayList<>();
    }

    public static NodeDataBase nodeToNodeDB(Node node) {
        List<TagDataBase> tags = node.getTag().stream()
            .map(tag -> TagDataBase.tagToTagDB(tag, Long.valueOf(String.valueOf(node.getId()))))
            .collect(Collectors.toList());
        return new NodeDataBase(Long.valueOf(String.valueOf(node.getId())), node.getUser(), Long.valueOf(String.valueOf(node.getUid())), Long.valueOf(String.valueOf(node.getVersion())), node.getLon(), node.getLat(), tags);
    }
}
