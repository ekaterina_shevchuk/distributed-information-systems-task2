# Distributed-information-systems-task2
1. Конструирование INSERT как строки и исполнение Statement.exequteQuery:
Time for insert: 2277697s. Speed: 2.193143776367094.
2. Использование PreparedStatement:
Time for insert: 2140376s. Speed: 2.333850220708885. 
3. Использование batch механизма и вставка порциями данных:
Time for insert: 242513s. Speed: 20.59814113057857. 

2 способ (Statement и PreparedStatement создаются при подключении к бд и получаются каждый раз при вставке в NodeDao и TagDao из класса DataBaseConnection):
1. Конструирование INSERT как строки и исполнение Statement.exequteQuery:
Time for insert: 2156824s. Speed: 2.316052213810677
2. Использование PreparedStatement:
Time for insert: 1588242s. Speed: 3.145186312917049 
3. Использование batch механизма и вставка порциями данных:
Time for insert: 294350s. Speed: 16.970670969933753

3 способ (Statement и PreparedStatement создаются при подключении к бд и передаются в NodeDao и TagDao из XMLReader):
1. Конструирование INSERT как строки и исполнение Statement.exequteQuery:
Time for insert: 3037247s. Speed: 1.64468579605149.
2. Использование PreparedStatement:
Time for insert: 1576777s. Speed: 3.1680554701140364.
3. Использование batch механизма и вставка порциями данных:
Time for insert: 236347s. Speed: 21.135521077060424. 
