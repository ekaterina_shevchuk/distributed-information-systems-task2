package dao;

import dataBase.DataBaseConnection;
import model.NodeDataBase;

import java.sql.*;
import java.util.List;

public class NodeDao {

    private static PreparedStatement prepareStatement(PreparedStatement statement, NodeDataBase node) throws SQLException {
        statement.setLong(1, node.getId());
        statement.setString(2, node.getUser());
        statement.setLong(3, node.getUid());
        statement.setLong(4, node.getVersion());
        statement.setDouble(5, node.getLongitude());
        statement.setDouble(6, node.getLatitude());
        return statement;
    }

    private static NodeDataBase mapNode(ResultSet resultSet) throws SQLException {
        return new NodeDataBase(resultSet.getLong("id"), resultSet.getString("username"), resultSet.getLong("uid"),
            resultSet.getLong("version"), resultSet.getDouble("longitude"), resultSet.getDouble("latitude"));
    }

    public void handleInsertNode(NodeDataBase node, Statement statement) throws SQLException {
        //Statement statement = DataBaseConnection.getStatementHandleNode();
        if (node.getUser().contains("'")){
            node.setUser(node.getUser().replace("'", "\""));
        }
        String sql = "insert into nodes(id, username, uid, version,  longitude, latitude) " +
            "values (" + node.getId() + ", '" + node.getUser() + "', " + node.getUid() + ", " + node.getVersion() + ", " + node.getLongitude() + ", " + node.getLatitude() + ")";
        statement.execute(sql);
    }

    public void prepareInsertNode(NodeDataBase node, PreparedStatement statement) throws SQLException {
        //PreparedStatement statement = DataBaseConnection.getStatementPrepareNode();
        statement = prepareStatement(statement, node);
        statement.execute();
    }

    public void batchInsertNodes(List<NodeDataBase> nodes, PreparedStatement statement) throws SQLException {
        //PreparedStatement statement = DataBaseConnection.getStatementPrepareNode();
        for (NodeDataBase node : nodes) {
            statement = prepareStatement(statement, node);
            statement.addBatch();
        }
        statement.executeBatch();
    }
}
