package dao;

import dataBase.DataBaseConnection;
import model.TagDataBase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TagDao {

    private static void prepareStatement(PreparedStatement statement, TagDataBase tag) throws SQLException {
        statement.setLong(1, tag.getNodeId());
        statement.setString(2, tag.getKey());
        statement.setString(3, tag.getValue());
    }

    private static TagDataBase mapTag(ResultSet rs) throws SQLException {
        return new TagDataBase(rs.getLong("node_id"), rs.getString("key"),
            rs.getString("value"));
    }

    public void handleInsertTag(TagDataBase tag, Statement statement) throws SQLException {
        //Statement statement = DataBaseConnection.getStatementHandleTag();
        if (tag.getValue().contains("'")){
            tag.setValue(tag.getValue().replace("'", "&"));
        }
        String sql = "insert into tags(node_id, key, value) " +
            "values (" + tag.getNodeId() + ", '" + tag.getKey() +
            "', '" + tag.getValue() + "')";
        statement.execute(sql);
    }

    public void prepareInsertPreparedTag(TagDataBase tag, PreparedStatement statement) throws SQLException {
        //PreparedStatement statement = DataBaseConnection.getStatementPrepareTag();
        prepareStatement(statement, tag);
        statement.execute();
    }

    public void batchInsertTags(List<TagDataBase> tags, PreparedStatement statement) throws SQLException {
        //PreparedStatement statement = DataBaseConnection.getStatementPrepareTag();
        for (TagDataBase tag : tags) {
            prepareStatement(statement, tag);
            statement.addBatch();
        }
        statement.executeBatch();
    }
}
