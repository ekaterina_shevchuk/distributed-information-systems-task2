package dataBase;

import dao.NodeDao;
import dao.TagDao;
import lombok.AllArgsConstructor;
import model.NodeDataBase;
import model.TagDataBase;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NodeService {
    private final NodeDao nodeDao = new NodeDao();
    private final TagDao tagDao = new TagDao();
    private final List<NodeDataBase> nodes = new ArrayList<>();

    public void handleInsertNode(NodeDataBase node, Statement statementNode, Statement statementTag) throws SQLException {
            nodeDao.handleInsertNode(node, statementNode);
            for (TagDataBase tag : node.getTags()) {
                tagDao.handleInsertTag(tag, statementTag);
            }
    }

    public void prepareInsertNode(NodeDataBase node, PreparedStatement statementNode, PreparedStatement statementTag) throws SQLException {
            nodeDao.prepareInsertNode(node, statementNode);
            for (TagDataBase tag : node.getTags()) {
                tagDao.prepareInsertPreparedTag(tag, statementTag);
            }
    }

    public void batchInsertNode(NodeDataBase node, PreparedStatement statementNode, PreparedStatement statementTag) throws SQLException {
        nodes.add(node);
        if (nodes.size() == 5000) {
            nodeDao.batchInsertNodes(nodes, statementNode);
            List<TagDataBase> tags = nodes.stream().flatMap(e -> e.getTags().stream()).collect(Collectors.toList());
            tagDao.batchInsertTags(tags, statementTag);
            nodes.clear();
        }
    }
}
