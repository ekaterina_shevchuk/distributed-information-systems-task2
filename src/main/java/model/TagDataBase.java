package model;

import lombok.Getter;
import lombok.Setter;
import model.generated.org.openstreetmap.osm._0.Tag;

@Getter
@Setter
public class TagDataBase {
    private final long nodeId;
    private final String key;
    private String value;

    public TagDataBase(long nodeId, String key, String value) {
        this.nodeId = nodeId;
        this.key = key;
        this.value = value;
    }

    public static TagDataBase tagToTagDB(Tag tag, long nodeId) {
        return new TagDataBase(nodeId, tag.getK(), tag.getV());
    }
}
