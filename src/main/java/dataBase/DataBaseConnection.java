package dataBase;

import java.io.*;
import java.sql.*;

public class DataBaseConnection {
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "password";

    private static final String SQL_INSERT_QUERY_NODE = "insert into nodes(id, username, uid, version, longitude, latitude) " +
        "values (?, ?, ?, ?, ?, ?)";

    private static final String SQL_INSERT_QUERY_TAG = "insert into tags(node_id, key, value) values (?, ?, ?)";

    private static Connection connection;
    private static Statement statementHandleNode;
    private static PreparedStatement statementPrepareNode;
    private static Statement statementHandleTag;
    private static PreparedStatement statementPrepareTag;

    public static void init(){
        File file = new File("src\\main\\resources\\init.sql");
        try {
            Reader reader = new BufferedReader(new FileReader(file));
            char[] sqlString = new char[1024];
            int readNum = reader.read(sqlString);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            Statement statement = connection.createStatement();
            String sql = new String(sqlString, 0, readNum);
            statement.execute(sql);

//            statementHandleNode = connection.createStatement();
//            statementPrepareNode = connection.prepareStatement(SQL_INSERT_QUERY_NODE);
//
//            statementHandleTag = connection.createStatement();
//            statementPrepareTag = connection.prepareStatement(SQL_INSERT_QUERY_TAG);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }

    public static Statement getStatementHandleNode() {
        return statementHandleNode;
    }

    public static PreparedStatement getStatementPrepareNode() {
        return statementPrepareNode;
    }

    public static Statement getStatementHandleTag() {
        return statementHandleTag;
    }

    public static PreparedStatement getStatementPrepareTag() {
        return statementPrepareTag;
    }

    public static void closeConnection() throws SQLException {
        if (connection != null)
            connection.close();
    }
}
