import dataBase.NodeService;
import model.NodeDataBase;
import model.generated.org.openstreetmap.osm._0.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.ref.SoftReference;
import java.sql.*;

public class XMLReader {
    private static final String NODE = "node";
    private static final String SQL_INSERT_QUERY_NODE = "insert into nodes(id, username, uid, version, longitude, latitude) " +
        "values (?, ?, ?, ?, ?, ?)";
    private static final String SQL_INSERT_QUERY_TAG = "insert into tags(node_id, key, value) values (?, ?, ?)";

    public static void parseXMLfile(String fileName, Connection connection) throws XMLStreamException, JAXBException, SQLException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = null;
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        NodeService nodeService = new NodeService();

        Statement statementHandleNode = connection.createStatement();
        PreparedStatement statementPrepareNode = connection.prepareStatement(SQL_INSERT_QUERY_NODE);

        Statement statementHandleTag = connection.createStatement();
        PreparedStatement statementPrepareTag = connection.prepareStatement(SQL_INSERT_QUERY_TAG);
        try {
            reader = xmlInputFactory.createXMLStreamReader(new FileInputStream(fileName));
            long start = System.currentTimeMillis();
            long count = 0;
            while (reader.hasNext()) {
                int xmlEvent = reader.next();
                if (XMLStreamConstants.START_ELEMENT == xmlEvent && NODE.equals(reader.getLocalName())){
                        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                        Node node = (Node) unmarshaller.unmarshal(reader);
                        nodeService.batchInsertNode(NodeDataBase.nodeToNodeDB(node),  statementPrepareNode, statementPrepareTag);       //Time for insert: 294350s. Speed: 16.970670969933753
                        //nodeService.prepareInsertNode(NodeDataBase.nodeToNodeDB(node), statementPrepareNode, statementPrepareTag);     //Time for insert: 1588242s. Speed: 3.145186312917049                        //nodeService.handleInsertNode(NodeDataBase.nodeToNodeDB(node));
                        //nodeService.handleInsertNode(NodeDataBase.nodeToNodeDB(node), statementHandleNode, statementHandleTag);      //Time for insert: 2156824s. Speed: 2.316052213810677
                    count++;
                }
            }
            long end = System.currentTimeMillis();
            end = end-start;
            double speed = (double)count/end;
            System.out.println("Time for insert: " + end + "s. Speed: " + speed + ". count = " + count);
        } catch (FileNotFoundException | XMLStreamException | SQLException exc) {
            exc.printStackTrace();
        } finally {
            reader.close();
        }
    }
}